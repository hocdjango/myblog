from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.contrib.sitemaps.views import sitemap

from apps.snippets.sitemaps import SnippetSitemap
from apps.blog.sitemaps import PostSitemap
from apps.blog.feeds import LatestPostsFeed
from apps.courses.sitemaps import CourseSiteMap
from apps.core.sitemaps import StaticSiteMap


sitemaps = {
    'snippets': SnippetSitemap,
    'posts': PostSitemap,
    'courses': CourseSiteMap,
    'static': StaticSiteMap,
}


urlpatterns = [
    path(settings.ADMIN_URL, admin.site.urls),
    path('accounts/', include('apps.users.urls')),
    path('accounts/', include('allauth.urls')),
    path('', include('apps.core.urls')),
    path('', include('apps.courses.urls')),
    path('', include('apps.blog.urls')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
            name='django.contrib.sitemaps.views.sitemap'),
    path('feed/', LatestPostsFeed(), name='post_feed'),
    path('', include('apps.orders.urls')),
    path('', include('apps.packages.urls')),
    path('snippets/', include('apps.snippets.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    try:
        import debug_toolbar
        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
    except ImportError:
        pass