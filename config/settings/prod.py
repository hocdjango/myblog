from .base import *

from decouple import config, Csv

# ==============================================================================
# CORE SETTINGS
# ==============================================================================

INSTALLED_APPS += [
	'storages',
]

if config("USER_POSTGRES", default="TRUE") == 'TRUE':
	INSTALLED_APPS += [
		'django.contrib.postgres'
	]

ADMINS = (
	('trungbat', 'hocdjango@gmail.com'),
)

# ==============================================================================
# SECURITY SETTINGS
# ==============================================================================

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_HSTS_SECONDS = 60 * 60 * 24 * 7 * 52
X_FRAME_OPTIONS = 'DENY'
SECURE_HSTS_PRELOAD = True
SECURE_REFERRER_POLICY=config('SECURE_REFERRER_POLICY', default='no-referrer')

# ==============================================================================
# CACHE SETTINGS
# ==============================================================================

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(os.path.dirname(BASE_DIR), 'cache'),
    }
}


# ==============================================================================
# THIRD-PARTY APPS SETTINGS
# ==============================================================================
