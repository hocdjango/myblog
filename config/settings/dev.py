from .base import *

# ==============================================================================
# CORE SETTINGS
# ==============================================================================

INSTALLED_APPS += [
    'debug_toolbar',
    'django_extensions',
]

INTERNAL_IPS = ["127.0.0.1", "10.0.2.2"]

import socket
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS += [ip[:-1] + "1" for ip in ips]


# ==============================================================================
# MIDDLEWARE SETTINGS
# ==============================================================================

MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')


# ==============================================================================
# EMAIL SETTINGS
# ==============================================================================

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# ==============================================================================
# CACHE SETTINGS
# ==============================================================================

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'posts_table',
    }
}