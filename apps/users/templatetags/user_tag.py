from django import template


register = template.Library()


@register.filter
def bg_color(user):
	colors = [
		'btn-primary', 'btn-secondary', 'btn-tertiary', 'btn-info',
		'btn-success', 'btn-warning', 'btn-dark', 'btn-gray', 'btn-light'
	]
	index = user.id % len(colors)

	return colors[index]



