from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.urls import reverse_lazy, reverse
from django.contrib.auth.forms import PasswordChangeForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import update_session_auth_hash
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from django.http import JsonResponse

from .forms import UserEditForm
from apps.snippets.models import Snippet
from apps.core.decorators import ajax_required
from apps.snippets.filters import SnippetFilter


@login_required
def account_edit(request):
    success = False
    if request.method == 'POST':
        form = UserEditForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            success = True
    else:
        form = UserEditForm(instance=request.user)

    context = {
        'user': request.user,
        'form': form,
        'section': 'account_edit',
        'success': success,
    }

    return render(request, 'users/edit.html', context)


@login_required
def my_courses(request):

    context = {
        'courses': 'courses',
        'section': 'my_courses',
    }

    return render(request, 'users/my_courses.html', context)


@login_required
def change_password(request):
    success = False
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            success = True
    else:
        form = PasswordChangeForm(request.user)

    context = {
        'success': success,
        'form': form,
    }

    return render(request, 'account/password_change.html', context)


@login_required
def snippets_management(request):
    query = Snippet.objects.filter(author=request.user)
    snippet_filter = SnippetFilter(request.GET, queryset=query)

    paginator = Paginator(snippet_filter.qs, 20)
    page = request.GET.get('page', 1)
    try:
        snippets = paginator.page(page)
    except PageNotAnInteger:
        snippets = paginator.page(1)
    except EmptyPage:
        snippets = paginator.page(paginator.num_pages)

    context = {
        'snippets': snippets,
        'form': snippet_filter.form,
        'section': 'snippets',
    }

    return render(request, 'users/snippets/manage.html', context)


@login_required
@ajax_required
def ajax_delete_snippet(request, snippet_slug):
    data = dict()
    snippet = get_object_or_404(Snippet, author=request.user, slug=snippet_slug)
    if request.method == 'POST':
        snippet.delete()
        data['is_valid'] = True
        snippets = Snippet.objects.filter(author=request.user)
        data['html_data'] = render_to_string('users/snippets/list.html',
                                            {'snippets': snippets},
                                            request=request)
    else:
        data['is_valid'] = False
        data['html_form'] = render_to_string('users/snippets/modal_delete.html',
                                            {'snippet': snippet},
                                            request=request)

    return JsonResponse(data)
