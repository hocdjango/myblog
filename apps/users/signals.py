from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import CustomUser, Student, Teacher


@receiver(post_save, sender=CustomUser)
def user_save(sender, instance, created, **kwargs):
    if created:
        if instance.is_student:
            Student.objects.create(user=instance)
        elif instance.is_teacher:
            Teacher.objects.create(user=instance)
    
    if not created:
        user_id = instance.id
        is_student = Student.objects.filter(user=user_id).first()
        is_teacher = Teacher.objects.filter(user=user_id).first()
        if instance.is_teacher and is_student:
            is_student.delete()
            Teacher.objects.create(user=instance)
        if instance.is_student and is_teacher:
            is_teacher.delete()
            Student.objects.create(user=instance)
        
