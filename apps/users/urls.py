from django.urls import path
from allauth.account import views as allauth_views

from . import views


urlpatterns = [
	path('edit/', views.account_edit, name='account_edit'),
	path('courses/', views.my_courses, name='my_courses'),
	path('password/change/', views.change_password, name="account_change_password"),
    # path('accounts/password/reset/', allauth_views.PasswordResetView.as_view(), name='account_reset_password'),

    path('accounts/snippets/', views.snippets_management, name='snippets_management'),
    path('ajax/snippet/<slug:snippet_slug>/', views.ajax_delete_snippet, name='ajax_delete_snippet'),
]
