from PIL import Image

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator


class CustomUser(AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+?\d{9,12}$')
    phone = models.CharField(max_length=12,
                            validators=[phone_regex],
                            blank=True)
    description = models.TextField(blank=True)

    is_student = models.BooleanField(default=True)
    is_teacher = models.BooleanField(default=False)

    def __str__(self):
        return self.get_name

    def get_word_avatar(self):
        if self.first_name and self.last_name:
            return ''.join((self.first_name[0], self.last_name[0])).upper()
        else:
            return self.username[0].upper()

    @property
    def get_name(self):
        if self.first_name or self.last_name:
            return ' '.join((self.last_name, self.first_name))
        return self.username
    
    @property
    def get_total_snippets(self):
        return self.snippets_created.all().count()

    @property
    def get_total_votes(self):
        return sum(snippet.total_rating for snippet in self.snippets_created.all())

        
class Teacher(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

class Student(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
