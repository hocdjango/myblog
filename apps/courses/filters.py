import django_filters

from django import forms
from django.db.models import Q

from .models import Course

from apps.core.models import Category


def filter_title_or_description(queryset, name, value):
	return queryset.filter(Q(title__contains=value)|Q(description__contains=value))


def filter_select(queryset, name, value):
	if value == 'new':
		return queryset.order_by('-created')
	elif value == 'pb':
		return queryset.order_by('-views')
	elif value == 'mp':
		return queryset.filter(price=None)
	elif value == 'cp':
		return queryset.exclude(price=None)

	return queryset


SELECT_CHOICES = (
	('new', 'Mới nhất'),
	('pb', 'Xem nhiều'),
	('mp', 'Miễn phí'),
	('cp', 'Trả phí'),
)


class CourseFilter(django_filters.FilterSet):
	query = django_filters.CharFilter(method=filter_title_or_description)
	categories = django_filters.ModelChoiceFilter(
								queryset=Category.objects.exclude(courses=None)\
								.filter(courses__status='published').distinct(),
										widget=forms.Select,
										empty_label='Tất cả')
	select = django_filters.ChoiceFilter(choices=SELECT_CHOICES,
										widget=forms.Select,
										method=filter_select,
										empty_label='Tất cả')

	class Meta:
		model = Course
		fields = ('categories', 'query', 'select',)
