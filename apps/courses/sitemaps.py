from django.contrib.sitemaps import Sitemap

from .models import Course


class CourseSiteMap(Sitemap):
	changefreq = 'weekly'
	priority = 0.9

	def items(self):
		return Course.published.all()

	def lastmod(self, obj):
		return obj.updated

