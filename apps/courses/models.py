import uuid

from django.urls import reverse
from django.conf import settings
from django.db import models
from django.utils.text import slugify

from .managers import CoursesdManager, PartManager
from apps.core.models import BaseContent, Category

from ckeditor_uploader.fields import RichTextUploadingField


class Course(BaseContent):
	STATUS_CHOICES = (
		('published', 'Công khai'),
		('draft', 'Chỉ mình tôi'),
	)
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, 
							on_delete=models.CASCADE,
							related_name='courses_created',
							null=True)
	categories = models.ManyToManyField(Category, related_name='courses')
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
	image = models.ImageField(upload_to='courses/')
	description = RichTextUploadingField(blank=True,
										null=True,
										config_name='special',
										external_plugin_resources=[(
                                          	'youtube',
                                          	'/static/vendor/youtube/youtube/',
                                          	'plugin.js',
                                         )],)
	views = models.PositiveIntegerField(default=0)
	price = models.DecimalField(max_digits=6,
								decimal_places=2,
								blank=True,
								null=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	
	objects = models.Manager()
	published = CoursesdManager()
	
	def get_absolute_url(self):
		return reverse('course_detail', args=[self.slug])

	class Meta:
		ordering = ('-created',)


class Part(BaseContent):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	course = models.ForeignKey(Course,
							related_name='parts',
							on_delete=models.CASCADE)

	objects = PartManager()
	
	class Meta:
		unique_together = ('title', 'course')

	def __str__(self):
		return f'{self.title} - {self.course.title}'
