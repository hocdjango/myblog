from django import template
from django.db.models import Count

from apps.blog.models import Post, Role


register = template.Library()



@register.inclusion_tag('courses/includes/posts_of_part.html')
def posts_of_part(part_id):
	return {
		'roles': Role.objects.filter(part__id=part_id)
	}

