import json

from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.conf import settings
from django.core.cache import cache

from .models import Course, Part
from .decorators import user_is_course_author, draft_for_author
from .filters import CourseFilter
from apps.blog.models import Post, Role
from apps.blog.forms import CommentForm
from apps.core.decorators import ajax_required
from django.views.decorators.cache import cache_page


# @cache_page(60 * 15)
def courses_list(request):
    query = Course.published.all()
    course_filter = CourseFilter(request.GET, queryset=query)

    paginator = Paginator(course_filter.qs, 6)
    page = request.GET.get('page')
    try:
        courses = paginator.page(page)
    except PageNotAnInteger:
        courses = paginator.page(1)
    except EmptyPage:
        courses = paginator.page(paginator.num_pages)

    context = {
        'courses': courses,
        'filter': course_filter,
    }
    
    return render(request, 'courses/course/list.html', context)


@draft_for_author
def course_detail(request, slug):
    course = get_object_or_404(Course, slug=slug)
    session_key = 'viewed_course_{}'.format(course.pk)
    if not request.session.get(session_key, False):
        course.views +=1
        course.save()
        request.session[session_key] = True

    context = {
        'course': course, 
    }

    return render(request, 'courses/course/detail.html', context)


def part_detail(request, part_id, post_slug=None):
    role = get_object_or_404(Role, part__id=part_id, post__slug=post_slug)
    post_pre = Post.objects.filter(parts__in=[role.part], role__order__lt=role.order).first()
    post_next = Post.objects.filter(parts__in=[role.part], role__order__gt=role.order).last()
    paginator = Paginator(role.post.comments.all(), settings.NUM_COMMENT_PAGE)
    comments = paginator.page(1)

    context = {
        'comment_form': CommentForm(),
        'post': role.post,
        'comments': comments,
        'part': role.part,
        'course': role.part.course,
        'post_pre': post_pre,
        'post_next': post_next,
    }

    return render(request, 'courses/part/detail.html', context)


@require_POST
@ajax_required
@user_is_course_author
def post_order(request, course_slug):
    if request.method == 'POST':

        data = json.loads(request.body)
        for id, order in data.items():
            Role.objects.filter(id=id, part__course__owner=request.user)\
                        .update(order=order)

    return JsonResponse({"saved": "OK"})

