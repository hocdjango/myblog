from django.contrib import admin

from .models import Course, Part

# from apps.blog.admin import PartInline


# class PostInline(admin.TabularInline):
# 	model = Post
# 	extra = 1


# @admin.register(Part)
# class PartAdmin(admin.ModelAdmin):
# 	list_display = ('title',)
# 	# inlines = [PostInline]


class PartInline(admin.TabularInline):
	model = Part
	extra = 1


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
	list_display = ('title', 'description', 'created',)
	list_filter = ('title',)
	date_hierarchy = 'created'
	inlines = [PartInline]
