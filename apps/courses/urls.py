from django.urls import path

from . import views


urlpatterns = [
	path('courses/', views.courses_list, name='courses_list'),
	path('course/<slug:course_slug>/order/', views.post_order, name='post_order'),
	path('course/<slug:slug>/', views.course_detail, name='course_detail'),
	path('course/<uuid:part_id>/<slug:post_slug>/', views.part_detail, name='part_detail'),
]
