from django.core.exceptions import PermissionDenied

from .models import Course


def user_is_course_author(f):
	def wrap(request, *args, **kwargs):
		course = Course.objects.get(slug=kwargs['course_slug'])
		if course.owner == request.user:
			return f(request, *args, **kwargs)
		else:
			raise PermissionDenied

	wrap.__doc__ = f.__doc__
	wrap.__name__ = f.__name__

	return wrap


def draft_for_author(f):
	def wrap(request, *args, **kwargs):
		course = Course.objects.get(slug=kwargs['slug'])
		if course.status == 'published' or course.owner == request.user:
			return f(request, *args, **kwargs)
		else:
			raise PermissionDenied

	wrap.__doc__ = f.__doc__
	wrap.__name__ = f.__name__

	return wrap