from django.db import models


class CoursesdManager(models.Manager):
    def get_queryset(self):
        return super(CoursesdManager,
                    self).get_queryset()\
                        .select_related('owner')\
                        .prefetch_related('categories')\
                        .filter(status='published')


class PartManager(models.Manager):
    def get_queryset(self):
        return super(PartManager,
                    self).get_queryset()\
                        .select_related('course')
