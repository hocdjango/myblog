
function getOS() {
  var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

  if (macosPlatforms.indexOf(platform) !== -1) {
    os = 'mac';
  } else if (iosPlatforms.indexOf(platform) !== -1) {
    os = 'mac';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    os = 'windows';
  } else if (/Android/.test(userAgent)) {
    os = 'linux';
  } else if (!os && /Linux/.test(platform)) {
    os = 'linux';
  }

  return os;
}


$(function() {

    //  set Platform
    var pls = ['linux', 'mac', 'windows']

    var os = getOS();
    $("button.platform[data-platform='" + os + "']").addClass("active");

    var showContent = function(p){
      $("button.platform[data-platform]").removeClass("active");
      $("button.platform[data-platform='" + p + "']").addClass("active");
      for (let e of pls){
        if (e==p){
          $("div.platform-" + e).show();
        } else {
          $("div.platform-" + e).hide();
        }
      }
    };

    showContent(os);

    $("button.platform[data-platform]").click(function(){
      showContent($(this).attr('data-platform'))
    })

    $("table").addClass("table");

    $('[data-toggle="tooltip"]').tooltip();

    // Scroll Table Of Content
    $(".module-post").each(function() {
        $(this).on("click", function(t) {
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top
            }, 700)
        })
    });

    $(".show-password").on("click", function() {
        let t = $(this).closest(".input-group").find("input");
        "password" == t.attr("type") ? t.attr("type", "text") : t.attr("type", "password")
    });

    $(".js-subscribe-form").on("submit", function() {
        let form = $(this);

        $.ajax({
            url: form.attr("action"),
            type: "post",
            dataType: "json",
            data: form.serialize(),
            success: function(t) {
                $(".msg").html("");
                t.success ? ($(".msg-success").html(t.message), $('.js-subscribe-form')[0].reset()) : $(".msg-error").html(t.message)
            }
        });
        return false;
    });


    /////////////////////////////////////////////////
    ///////////////// MODAL  
    /////////////////////////////////////////////////

    var loadForm = function () {
        let el = $(this);

        $.ajax({
            url: el.attr('data-url'),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-root").modal("show");
            },
            success: function (data) {
                 $('#modal-root .modal-content').html(data.html_form);
            }
        })
    };

    var submitRedirectForm = function () {
        let form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                if (data.is_valid) {
                   window.location.href = data.redirect_url
                   $("#modal-root").modal("hide");
                } else {
                    $('#modal-root .modal-content').html(data.html_form);
                }
            }
        });

        return false;
    };
    

    //  Copy Snippet
    var copySnippet = function () {
      var el = $(this);

      $.ajax({
        url: el.attr('data-url'),
        dataType: 'json',
        type: 'get',
        success: function (data) {
          var copyTest = document.queryCommandSupported('copy');
          var elOriginalText = el.attr('data-original-title');

          if (copyTest) {
            const new_el = document.createElement('textarea');
            new_el.value = data.data_copy;
            document.body.appendChild(new_el);
            new_el.select();
            document.execCommand('copy');
            document.body.removeChild(new_el);

            el.attr('data-original-title', 'Đã copy!')
                      .tooltip('show');
            el.attr('data-original-title', elOriginalText)
          }
        },
      });

      return false;
    };


    // GET METHOD

    $("#modal-data").on('click', 'button.btn-modal', loadForm);
    $("#modal-data").on('click', 'a.btn-modal', loadForm);
    

    // POST METHOD

    // Delete Snippet
    $('#modal-root').on("submit", '#js-form-doNothing', submitRedirectForm);



    /////////////////////////////////////////////////
    ///////////////// COPY SNIPPET
    /////////////////////////////////////////////////

    $("#modal-data").on('click', 'a.js-copy-snippet', copySnippet);



});