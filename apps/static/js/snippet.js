$(function () {

  var getOption = function () {
      var language = $('#id_language').children("option:selected").val();
      var mode = `ace/mode/${ language }`;
      
      var sizes = {
          django: 2,
          python: 4,
          css: 4,
          sql: 4,
          powershell: 2,
      }
      option = {
        mode: mode,
        wrap: true,
        tabSize: sizes[language],
        useSoftTabs: true
      }

      return option;
  }

  var editor = ace.edit("snippetEditor");
  editor.setTheme("ace/theme/dracula");

  editor.setValue($('#id_code').val());
  editor.getSession().setOptions( getOption() );
  editor.setOptions({
        maxLines: Infinity
  });

  editor.on("change", function (e) {
      $("#id_code").val(editor.getValue());
  });

  $('#id_language').on('change', function () {
    editor.getSession().setOptions( getOption() );
  });

  // Set Font-Size
  $('.ace_editor').each(function( index ) {
     edi = ace.edit(this);
     edi.setFontSize("1em");
  });

  $('form').submit(function () {
    $("#id_code").val(editor.getValue());
    return true;
  });

});