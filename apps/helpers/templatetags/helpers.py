from django import template


register = template.Library()


@register.filter
def pagination(page, current, delta=2):
    """Trả vè là một list 
    
    Keyword arguments:
    page -- Pagination trong Django
    current -- page.page_number là số page hiện tại
    
    - pagination [10,  20] -> [1, 0, 8, 9, 10, 11, 12, 0, 20]
    """
    _range = []
    range_with_dots = []
    l = None

    for i in page.paginator.page_range:
        if (
            i == 1 or i == page.paginator.num_pages
            or i >= (current - delta)
            and i <= (current + delta)
        ):
            _range.append(i)
    
    for i in _range:
        if l is not None:
            if i - l == 2:
                print(l + 1)
                range_with_dots.append(l + 1)
            elif i - l != 1:
                range_with_dots.append(0)
        range_with_dots.append(i)
        l = i
    return range_with_dots
