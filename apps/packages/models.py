from django.db import models
from django.utils.text import slugify


class Topic(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, blank=True, null=True)

    def __str__(self):
        return f'{ self.name }'

    def save(self):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Topic, self).save()