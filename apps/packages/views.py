import requests
from urllib.parse import quote_plus, urlencode

from django.shortcuts import render

from .forms import SearchForm


def package_list(request):
    form = SearchForm(request.GET)

    q = request.GET.get('q')
    sort = request.GET.get('sort')
    topic = request.GET.get('topic')
    page = request.GET.get('page')

    sort = sort if sort else ''
    q = quote_plus(q) if q else ''
    tp = ''.join(('topic:', topic)) if topic else ''
    page = page if page else 1

    if topic:
        query = '+'.join((q, tp))
    else:
        query = q if q else 'django'

    url = f'https://api.github.com/search/repositories?q={ query }&sort={ sort }&page={ page }'

    try:
        response = requests.get(url)
        repodata = response.json()['items']
    except:
        url = f'https://api.github.com/search/repositories?q=django'
        response = requests.get(url)
        repodata = response.json()['items']
           
    context = {
        'form': form,
        'repositories': repodata,
        'current_page': page,
    }

    return render(request, 'packages/package_list.html', context)


