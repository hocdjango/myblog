import math

from django import template


register = template.Library()


@register.filter
def round_up(n, decimals=1):
    stuf = ''
    if n >= 1000000:
        n = n/1000000
        stuf = 'M'
    if n >= 1000:
        n = n/1000
        stuf = 'K'
    multiplier = 10 ** decimals

    return str(math.ceil(n * multiplier) / multiplier) + stuf