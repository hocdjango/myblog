from django import forms

from .models import Topic


MATCH = ''
STARS = 'starts'
FORKS = 'forks'
UPDATED = 'updated'

ORDER_CHOICES = (
    (MATCH, 'Best Match'),
    (STARS, "stars"),
    (FORKS, "forks"),
    (UPDATED, "updated"),
)

TOPIC_CHOICES = [('', 'Tất cả')] + [(topic.slug, topic.name) for topic in Topic.objects.all()]

class SearchForm(forms.Form):
    page = forms.IntegerField(initial=1)
    q = forms.CharField(max_length=100, initial='django')
    topic = forms.ChoiceField(choices=TOPIC_CHOICES)
    sort = forms.ChoiceField(choices=ORDER_CHOICES, initial=MATCH, required=False)
    


