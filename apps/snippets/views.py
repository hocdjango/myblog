import mimetypes

from django.views.decorators.csrf import ensure_csrf_cookie
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt

from .models import Snippet
from .forms import SnippetCreateForm
from .constants import FileFormats
from .filters import SnippetFilter, UserFilter, TagFilter
from apps.core.decorators import ajax_required
from taggit.models import Tag


User = get_user_model()


def snippets_list(request, query, section):
    snippet_filter = SnippetFilter(request.GET, queryset=query)

    paginator = Paginator(snippet_filter.qs, 20)
    page = request.GET.get('page', 1)
    try:
        snippets = paginator.page(page)
    except PageNotAnInteger:
        snippets = paginator.page(1)
    except EmptyPage:
        snippets = paginator.page(paginator.num_pages)

    context = {
        'snippets': snippets,
        'form': snippet_filter.form,
        'section': section,
    }

    return render(request, 'snippets/snippet/list.html', context)


def snippets_list_news(request):
    section = 'news'
    query = Snippet.objects.all()
    return snippets_list(request, query, section)


def snippets_list_by_rating(request):
    section = 'rating'
    query = Snippet.objects.order_by('-total_rating', '-created')
    return snippets_list(request, query, section)


def snippets_list_by_author(request, username):
    section = 'users'
    query = Snippet.objects.filter(author__username=username)
    return snippets_list(request, query, section)


def snippets_list_by_tag(request, tag_slug):
    tag = get_object_or_404(Tag, slug=tag_slug)
    section = 'tag'
    query = Snippet.objects.filter(tags__in=[tag])
    return snippets_list(request, query, section)


@login_required
def snippets_list_saved(request):
    section = 'saved'
    query = Snippet.objects.filter(users__in=[request.user])
    return snippets_list(request, query, section)


def snippet_tag_list(request):
    query = Tag.objects.annotate(total_snippets=Count('sippets_taged')).order_by('-total_snippets').all()

    tag_filter = TagFilter(request.GET, queryset=query)

    paginator = Paginator(tag_filter.qs, 20)
    page = request.GET.get('page', 1)

    try:
        tags = paginator.page(page)
    except PageNotAnInteger:
        tags = paginator.page(1)
    except EmptyPage:
        tags = paginator.page(paginator.num_pages)

    context = {
        'tags': tags,
        'form': tag_filter.form,
        'section': 'tag',
    }

    return render(request, 'snippets/snippet/list_tags.html', context)


def snippet_user_list(request):
    query = User.objects.prefetch_related('snippets_created')\
                        .annotate(total_snippets=Count('snippets_created'))\
                        .order_by('-total_snippets')

    user_filter = UserFilter(request.GET, queryset=query)

    paginator = Paginator(user_filter.qs, 20)
    page = request.GET.get('page', 1)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    context = {
        'users': users,
        'form': user_filter.form,
        'section': 'users',
    }

    return render(request, 'snippets/snippet/list_users.html', context)


def snippet_detail(request, snippet_slug):
    snippet = get_object_or_404(Snippet, slug=snippet_slug)

    session_key = 'viewed_snippet_{}'.format(snippet.pk)
    if not request.session.get(session_key, False):
        snippet.views +=1
        snippet.save()
        request.session[session_key] = True

    context = {
        'snippet': snippet,
        'section': 'news',
    }

    return render(request, 'snippets/snippet/detail.html', context)


@login_required
def snippet_create(request):
    if request.method == 'POST':
        form = SnippetCreateForm(request.POST)
        if form.is_valid():
            new_snippet = form.save(commit=False)
            new_snippet.author = request.user
            new_snippet.save()
            form.save_m2m()
            return redirect('snippet_detail', new_snippet.slug)
    else:
        form = SnippetCreateForm()

    context = {
        'form': form,
        'section': 'create_snippet'
    }

    return render(request, 'snippets/snippet/create.html', context)


@login_required
def snippet_update(request, snippet_slug):
    snippet = get_object_or_404(Snippet, author=request.user, slug=snippet_slug)

    if request.method == 'POST':
        form = SnippetCreateForm(request.POST, instance=snippet)
        if form.is_valid():
            form.save()
            return redirect('snippet_detail', snippet.slug)
    else:
        form = SnippetCreateForm(instance=snippet)

    context = {
        'form': form,
        'snippet': snippet,
        'section': 'news',
    }

    return render(request, 'snippets/snippet/update.html', context)


@ajax_required
@login_required
def ajax_snippet_delete(request, snippet_slug):
    data = dict()
    snippet = get_object_or_404(Snippet, author=request.user, slug=snippet_slug)
    
    if request.method == 'POST':
        snippet.delete()
        data['is_valid'] = True
        data['redirect_url'] = reverse('snippets_list_news')
    else:
        data['is_valid'] = False
    
    data['html_form'] = render_to_string('snippets/snippet/modal_delete.html',
                                        {'snippet': snippet},
                                        request=request)

    return JsonResponse(data)


def snippet_raw(request, snippet_slug):
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    response = HttpResponse(snippet.code, content_type="text/plain; charset=utf-8")

    return response


@ajax_required
def copy_snippet(request, snippet_slug):
    data = dict()
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    data['data_copy'] = snippet.code

    return JsonResponse(data)


@login_required
@ajax_required
@require_POST
def save_snippet(request, snippet_slug):
    data = dict()
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    if request.user in snippet.users.all():
        snippet.users.remove(request.user)
        data['saved'] = False
    else:
        snippet.users.add(request.user)
        data['saved'] = True

    data['html_data'] = render_to_string('snippets/snippet/save_snippet_icon.html',
                                        {"snippet": snippet},
                                        request=request)

    return JsonResponse(data)


def download_snippet(request, snippet_slug):
    data = dict()
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    file_name = slugify(snippet.title) + FileFormats.LABELS[snippet.language]
    mime_type, _ = mimetypes.guess_type(file_name)

    response = HttpResponse(snippet.code, content_type=mime_type)
    response['Content-Disposition'] = f'attachment; filename="{file_name}"'

    return response


def ajax_vote_snippet(request, snippet):
    data = dict()
    data['html_data'] = render_to_string('snippets/snippet/votes.html',
                                        {'snippet': snippet},
                                        request=request)

    return JsonResponse(data)


@login_required
@ajax_required
@require_POST
def ajax_vote_up_snippet(request, snippet_slug):
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    snippet.create_vote_up(request.user)

    return ajax_vote_snippet(request, snippet)


@login_required
@ajax_required
@require_POST
def ajax_vote_down_snippet(request, snippet_slug):
    snippet = get_object_or_404(Snippet, slug=snippet_slug)
    snippet.create_vote_down(request.user)

    return ajax_vote_snippet(request, snippet)
