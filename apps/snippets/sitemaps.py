from django.contrib.sitemaps import Sitemap
from .models import Snippet


class SnippetSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        return Snippet.objects.all()
