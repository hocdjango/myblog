


class LanguageTypes:
    PYTHON = 'python'
    HTML = 'django'
    JAVASCRIPT = 'javascript'
    SHELL = 'powershell'
    CSS = 'css'
    SQL = 'sql'
    
    LABELS = {
        PYTHON: 'Python',
        HTML: 'Html/Template',
        JAVASCRIPT: 'Javascript',
        SHELL: 'Shell',
        CSS: 'CSS',
        SQL: 'SQL',
    }

    CHOICES = tuple(LABELS.items())


class FileFormats:
    PYTHON = 'python'
    HTML = 'django'
    JAVASCRIPT = 'javascript'
    SHELL = 'powershell'
    CSS = 'css'
    SQL = 'sql'

    LABELS = {
        PYTHON: '.py',
        HTML: '.html',
        JAVASCRIPT: '.js',
        SHELL: '.txt',
        CSS: '.css',
        SQL: '.sql',
    }

    CHOICES = tuple(LABELS.items())

