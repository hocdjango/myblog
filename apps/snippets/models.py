from django.urls import reverse
from django.db import models
from django.conf import settings

from .manages import SnippetManager
from .constants import LanguageTypes
from apps.core.models import BaseContent

from taggit.managers import TaggableManager


class Snippet(BaseContent):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='snippets_created',
                                on_delete=models.CASCADE)
    tags = TaggableManager(related_name='sippets_taged')
    created = models.DateTimeField(auto_now_add=True)
    code = models.TextField()
    description = models.TextField(blank=True)
    language = models.CharField(
                    max_length=20,
                    choices=LanguageTypes.CHOICES,
                    default=LanguageTypes.PYTHON,
            )
    vote_up = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='voteup_created')
    vote_down = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='votedown_created')
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='snippets_saved')
    views = models.PositiveIntegerField(default=0)

    objects = SnippetManager()

    class Meta:
        ordering = ('-created', )

    def get_absolute_url(self):
        return reverse('snippet_detail', args=[self.slug])

    @property
    def get_tags(self):
        return self.tags.all()[:5]

    @property
    def get_total_vote_up(self):
        return self.vote_up.all().count()

    @property
    def get_total_vote_down(self):
        return self.vote_down.all().count()

    def user_voted(self, user, up=True):
        is_user_up = user in self.vote_up.all()
        is_user_down = user in self.vote_down.all()

        self.vote_up.remove(user)
        self.vote_down.remove(user)

        return is_user_up if up else is_user_down
        

    def create_vote_up(self, user):
        if not self.user_voted(user):
            self.vote_up.add(user)

    def create_vote_down(self, user):
        if not self.user_voted(user, False):
            self.vote_down.add(user)


