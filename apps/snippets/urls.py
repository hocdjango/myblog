from django.urls import path

from . import views


urlpatterns = [
    path('news/', views.snippets_list_news, name='snippets_list_news'),
    path('rating/', views.snippets_list_by_rating, name='snippets_list_by_rating'),
    path('author/<str:username>/', views.snippets_list_by_author, name='snippets_list_by_author'),
    path('tags/<slug:tag_slug>/', views.snippets_list_by_tag, name='snippets_list_by_tag'),
    path('saved/', views.snippets_list_saved, name='snippets_list_saved'),
    path('users/', views.snippet_user_list, name='snippet_user_list'),
    path('tags/', views.snippet_tag_list, name='snippet_tag_list'),

    path('snippet/<slug:snippet_slug>/', views.snippet_detail, name='snippet_detail'),

    path('copy/<slug:snippet_slug>/', views.copy_snippet, name='copy_snippet'),
    path('save/<slug:snippet_slug>/', views.save_snippet, name='save_snippet'),
    path('download/<slug:snippet_slug>/', views.download_snippet, name='download_snippet'),

    path('ajax/vote/up/<slug:snippet_slug>/', views.ajax_vote_up_snippet, name='ajax_vote_up_snippet'),
    path('ajax/vote/down/<slug:snippet_slug>/', views.ajax_vote_down_snippet, name='ajax_vote_down_snippet'),

    path('create', views.snippet_create, name='snippet_create'),
    path('<slug:snippet_slug>/update/', views.snippet_update, name='snippet_update'),
    path('<slug:snippet_slug>/delete/', views.ajax_snippet_delete, name='ajax_snippet_delete'),

    path('snippet/<slug:snippet_slug>/raw', views.snippet_raw, name='snippet_raw'),
]
