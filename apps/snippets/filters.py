import django_filters
from django.contrib.auth import get_user_model
from django.db.models import Q

from .models import Snippet
from taggit.models import Tag


class SnippetFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Snippet
        fields = ('title', )


def search_user(queryset, name, value):
    return queryset.filter(
                Q(username__contains=value) |
                Q(first_name__contains=value) |
                Q(last_name__contains=value) |
                Q(email__contains=value))


class UserFilter(django_filters.FilterSet):
    query = django_filters.CharFilter(method = search_user)

    class Meta:
        model = get_user_model()
        fields = ('query', )


class TagFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Tag
        fields = ('name', )
