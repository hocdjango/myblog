from django.db import models
from django.db.models import Count


class SnippetManager(models.Manager):
    def get_queryset(self):
        return super(SnippetManager, self).get_queryset()\
                    .select_related('author')\
                    .prefetch_related('tags', 'vote_up', 'vote_down')\
                    .annotate(total_rating=Count('vote_up', distinct=True) - Count('vote_down', distinct=True))
