from django.db import models


class PostManager(models.Manager):
    def get_queryset(self):
        return super(PostManager,
                    self).get_queryset()\
                        .select_related('owner')\
                        .prefetch_related('category', 'tags', 'modules')\
                        .filter(status='published')\
                        .filter(parts=None)
