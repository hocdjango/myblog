# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from django.core.mail import send_mail
# from django.template.loader import render_to_string
# from django.utils.html import strip_tags
# from django.contrib.sites.models import Site
# from django.conf import settings
# from django.contrib.auth import get_user_model

# from .models import Post
# from apps.courses.models import Course, Part

# from apps.core.models import Subscribe


# @receiver(post_save, sender=Course)
# @receiver(post_save, sender=Post)
# def post_save(sender, instance, created, **kwargs):
# 	if created:
# 		context = {}
# 		if instance.status == 'published':
# 			if sender.__name__ == 'Course':
# 				context['link_text'] = 'Xem khóa học'
# 				context['img_link'] = instance.image.url
# 				context['link'] = instance.get_absolute_url()
# 				context['overview'] = ''
# 				context['title'] = instance.title
# 			elif sender.__name__ == 'Post':
# 				context['link_text'] = 'Xem bài viết'
# 				context['title'] = instance.title
# 				context['overview'] = instance.overview
# 				context['link'] = instance.get_absolute_url()
# 				img = instance.image
# 				context['img_link'] = img.url if img else ''

# 			domain = Site.objects.get_current().domain
# 			context['link'] = ''.join(('http://', domain, context['link']))
			
# 			if context['img_link']:
# 				subject = context['title']
# 				html_message = render_to_string('core/email/post.html', context)
# 				plain_message = strip_tags(html_message)
# 				from_email = 'djangobat <hocdjango@gmail.com>'
# 				emails = get_user_model().objects.exclude(email="hocdjango@gmail.com").values_list('email', flat=True)
# 				subscribes = Subscribe.objects.values_list('email', flat=True)
# 				emails_to = list(emails.union(subscribes))
# 				try:
# 					send_mail(subject, plain_message, from_email, emails_to, html_message=html_message)
# 				except:
# 					pass