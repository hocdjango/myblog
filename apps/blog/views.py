from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_page

from .models import Post, Comment
from .filters import PostFilter
from .forms import CommentForm
from apps.core.decorators import ajax_required
from .decorators import is_author_comment

from taggit.models import Tag


# @cache_page(60 * 15)
def post_list(request, tag_slug=None):
	query = Post.objects.prefetch_related('tags', 'parts', 'modules')\
						.filter(parts=None, status='published')

	if tag_slug:
		tag = get_object_or_404(Tag, slug=tag_slug)
		query = query.filter(tags__in=[tag])

	post_filter = PostFilter(request.GET, queryset=query)

	paginator = Paginator(post_filter.qs, 5)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	context = {
		'posts': posts,
		'filter': post_filter,
	}

	return render(request, 'blog/post/list.html', context)


def post_detail(request, slug):
	post = get_object_or_404(Post, slug=slug)
	post_tags_ids = post.tags.values_list('id', flat=True)
	similar_posts = Post.published.filter(tags__in=post_tags_ids)\
								.exclude(id=post.id)
	if similar_posts.exists():
		similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
									.order_by('-same_tags', '-created')[:3]
	else:
		similar_posts = Post.published.exclude(id=post.id)[:3]

	session_key = 'viewed_post_{}'.format(post.pk)
	if not request.session.get(session_key, False):
		post.views +=1
		post.save()
		request.session[session_key] = True

	session_key = 'like_post_{}'.format(post.pk)

	context = {
		'post': post,
		'similar_posts': similar_posts,
		'like': request.session.get(session_key, False),
	}

	return render(request, 'blog/post/detail.html', context)


@ajax_required
@require_POST
def ajax_update_post_like(request, post_slug):
	data = {}
	post = get_object_or_404(Post, slug=post_slug)

	if request.method == 'POST':
		session_key = 'like_post_{}'.format(post.pk)
		if not request.session.get(session_key, False):
			post.like +=1
			post.save()
			request.session[session_key] = True
		else:
			post.like -=1
			post.save()
			request.session[session_key] = False

		data['html_form'] = render_to_string('blog/includes/form_like.html',
											{'post': post, 'like': request.session[session_key]},
											request=request)
		data['post_likes'] = post.like

		return JsonResponse(data)


@login_required
@ajax_required
@require_POST
def create_comment(request, post_id):
	if request.method == "POST":
		post = get_object_or_404(Post, id=post_id)
		form = CommentForm(request.POST)
		if form.is_valid():
			new_comment = form.save(commit=False)
			new_comment.user = request.user
			new_comment.post = post
			new_comment.save()
			html_comment = render_to_string('blog/comment/detail.html',
												{'comment': new_comment},
												request=request)
			data = {'is_valid': True, 'html_comment': html_comment, 'num_comments': post.comments.count()}
		else:
			data = {'is_valid': False}

		return JsonResponse(data)


@login_required
@ajax_required
@is_author_comment
def remove_comment(request, comment_id):
	data = {}
	comment = get_object_or_404(Comment, id=comment_id)
	if request.method == 'POST':
		comment.delete()
		if comment.reply:
			comments = comment.reply.post.comments.all()
			data['num_comments'] = comments.count()
		else:
			comment_lists = comment.post.comments.all()
			paginator = Paginator(comment_lists, settings.NUM_COMMENT_PAGE)
			try:
				page = int(request.POST.get('page')) - 1
				if 1 <= page <= paginator.num_pages:
					num_cmt = page*settings.NUM_COMMENT_PAGE
					comments = comment_lists[:num_cmt]
					
				data['num_comments'] = comment_lists.count()
				
			except Exception as e:
				data = {'is_valid': False}

		data['is_valid'] = True
		data['html_data'] = render_to_string('blog/comment/list.html',
											{'comments': comments},
											request=request)

	else:
		html_form = render_to_string('blog/comment/remove.html',
							{'comment': comment},
							request=request)
		data['html_form'] = html_form

	return JsonResponse(data)


@login_required
@ajax_required
@is_author_comment
def comment_update(request, comment_id):
	data = {}
	comment = get_object_or_404(Comment, id=comment_id)
	if request.method == 'POST':
		form = CommentForm(request.POST, instance=comment)
		if form.is_valid():
			comment_updated = form.save()
			if comment.reply:
				comments = comment.reply.post.comments.all()
				data['num_comments'] = comments.count()
			else:
				comment_lists = comment.post.comments.all()
				paginator = Paginator(comment_lists, settings.NUM_COMMENT_PAGE)
				try:
					page = int(request.POST.get('page')) - 1
					
					if 1 <= page <= paginator.num_pages:
						num_cmt = page*settings.NUM_COMMENT_PAGE
						comments = comment_lists[:num_cmt]

					data['num_comments'] = comment_lists.count()

				except Exception as e:
					data = {'is_valid': False}
					return JsonResponse(data)

			data['is_valid'] = True
			data['html_data'] = render_to_string('blog/comment/list.html',
												{'comments': comments},
												request=request)
		else:
			data = {'is_valid': False}
	else:
		form = CommentForm(instance=comment)
		html_form = render_to_string('blog/comment/edit.html',
							{'form': form, 'comment': comment},
							request=request)
		data['html_form'] = html_form

	return JsonResponse(data)


@require_POST
@login_required
@ajax_required
def like_comment(request, comment_id):
	print(request.POST)
	comment = get_object_or_404(Comment, id=comment_id)
	if request.method == 'POST':
		comment.create_like(request.user)

		html_data = render_to_string('blog/comment/includes/like.html',
									{'comment': comment},
									request=request)

		data = {'html_data': html_data}

		return JsonResponse(data)


@require_POST
@login_required
@ajax_required
def dislike_comment(request, comment_id):
	comment = get_object_or_404(Comment, id=comment_id)
	if request.method == 'POST':
		comment.create_dislike(request.user)
		
		html_data = render_to_string('blog/comment/includes/like.html',
									{'comment': comment},
									request=request)

		data = {'html_data': html_data}

		return JsonResponse(data)


@require_POST
@login_required
@ajax_required
def comment_reply(request, comment_id):
	comment = get_object_or_404(Comment, id=comment_id)
	if request.method == "POST":
		form = CommentForm(request.POST)
		if form.is_valid():
			new_reply = form.save(commit=False)
			new_reply.user = request.user
			new_reply.reply = comment
			new_reply.save()
			html_data = render_to_string('blog/comment/reply.html',
									{'reply': new_reply},
									request=request)
			data = {'is_valid': True, 'html_data': html_data}
		else:
			data = {'is_valid': False}

		return JsonResponse(data)


@csrf_exempt
@require_POST
@ajax_required
def load_comment(request, post_id):
    data = {}
    post = get_object_or_404(Post, id=post_id)
    comment_lists = post.comments.all()
    paginator = Paginator(comment_lists, settings.NUM_COMMENT_PAGE)
    page = request.POST.get('page')

    try:
        comments = paginator.page(page)
        data['is_valid'] = True
        data['html_data'] = render_to_string('blog/comment/list.html',
                                        	{'comments': comments},
                                        	request=request)
        data['has_next'] = comments.has_next()
    except:
    	data['is_valid'] = False

    return JsonResponse(data)

