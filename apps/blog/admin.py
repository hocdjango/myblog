from django.contrib import admin

from .models import Post, Module, Role, Comment


class ModuleInline(admin.TabularInline):
	model = Module


class RoleInline(admin.StackedInline):
	model = Role
	

admin.site.register(Comment)
# class CommentInline(admin.TabularInline):
# 	model = Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	list_display = ('title', 'created', 'status',)
	list_filter = ('title','status')
	date_hierarchy = 'created'
	inlines = [RoleInline, ModuleInline]
