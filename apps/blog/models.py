from django.urls import reverse
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.db.models import Count
from django.utils.html import mark_safe

from .managers import PostManager
from apps.core.models import BaseContent, Category
from apps.core.fields import OrderField
from apps.courses.models import Part

from taggit.managers import TaggableManager
from ckeditor_uploader.fields import RichTextUploadingField
from markdown import markdown


class Post(BaseContent):
    STATUS_CHOICES = (
        ('published', 'Công khai'),
        ('draft', 'Chỉ mình tôi'),
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, 
                            on_delete=models.CASCADE,
                            related_name='posts_created')
    category = models.ManyToManyField(Category,
                                related_name='posts')
    image = models.ImageField(upload_to='posts/', blank=True)
    overview = RichTextUploadingField(blank=True,
                                    null=True,
                                    config_name='special',
                                    external_plugin_resources=[(
                                        'youtube',
                                        '/static/vendor/youtube/youtube/',
                                        'plugin.js',
                                     )],)
    status = models.CharField(max_length=10,
                            choices=STATUS_CHOICES,
                            default='draft')
    views = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    like = models.PositiveIntegerField(default=0)
    parts = models.ManyToManyField(to=Part,
                            through='Role',
                            related_name='posts_of_part',
                            blank=True)

    tags = TaggableManager(blank=True, related_name='post_taged')

    objects = models.Manager()
    published = PostManager()

    def get_absolute_url(self):
        return reverse('post_detail', args=[self.slug])

    @property
    def count_comments(self):
        return self.comments.filter(active=True).count()

    class Meta:
        ordering = ('-created',)
        

class RoleManager(models.Manager):
    def get_queryset(self):
        return super(RoleManager,
                    self).get_queryset()\
                        .select_related('part', 'post')


class Role(models.Model):
    part = models.ForeignKey(Part, on_delete=models.DO_NOTHING)
    post = models.ForeignKey(Post, on_delete=models.DO_NOTHING)
    order = OrderField(blank=True, for_fields=['part'])

    objects = RoleManager()

    class Meta:
        ordering = ('order',)
        
    def __str__(self):
        if self.order:
            return f'{self.order}: {self.post.title}'
        else:
            return f'Role of {self.post.title}'


class Module(BaseContent):
    description = RichTextUploadingField(blank=True,
                                        null=True,
                                        config_name='special',
                                        external_plugin_resources=[(
                                            'youtube',
                                            '/static/vendor/youtube/youtube/',
                                            'plugin.js',
                                         )],)
    created = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post,
                            on_delete=models.CASCADE,
                            related_name='modules')
     
    class Meta:
        ordering = ('created',)


class CommentManager(models.Manager):
    def get_queryset(self):
        return super(CommentManager,
                    self).get_queryset()\
                        .select_related('post', 'user', 'reply')\
                        .prefetch_related('like', 'dislike')\
                        .filter(active=True)\
                        .annotate(num_like=Count('like') - Count('dislike'))\
                        .order_by('-num_like', '-created')


class Comment(models.Model):
    post = models.ForeignKey(Post,
                            related_name='comments',
                            on_delete=models.CASCADE,
                            blank=True,
                            null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            on_delete=models.CASCADE,
                            related_name='comments_created')
    like = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                    related_name='likes',
                                    blank=True)
    dislike = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                    related_name='dislikes',
                                    blank=True)
    active = models.BooleanField(default=True)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    reply = models.ForeignKey('self',
                            on_delete=models.CASCADE,
                            related_name='replies',
                            blank=True,
                            null=True)

    objects = CommentManager()
    
    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f"{self.user.username} comment { self.content }"
    
    def get_content_as_markdown(self):
        return mark_safe(markdown(self.content, safe_mode='escape'))

    def create_actions(self, user, like=True):
        is_liked = user in self.like.all()
        is_disliked = user in self.dislike.all()

        self.like.remove(user)
        self.dislike.remove(user)

        return is_liked if like else is_disliked

    def create_like(self, user):
        if not self.create_actions(user):
            self.like.add(user)

    def create_dislike(self, user):
        if not self.create_actions(user, False):
            self.dislike.add(user)
