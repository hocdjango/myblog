from django.core.exceptions import PermissionDenied

from .models import Comment


def is_author_comment(f):
	def wrap(request, *args, **kwargs):
		comment = Comment.objects.get(id=kwargs['comment_id'])
		if comment.user == request.user:
			return f(request, *args, **kwargs)
		else:
			raise PermissionDenied

	wrap.__doc__ = f.__doc__
	wrap.__name__ = f.__name__

	return wrap