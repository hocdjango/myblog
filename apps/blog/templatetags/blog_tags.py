from django import template
from django.db.models import Count

from ..models import Post
from apps.core.forms import SubscribeForm
from apps.snippets.models import Snippet

from taggit.models import Tag


register = template.Library()


@register.filter
def count_posts_by_tag(tag):
    return Post.published.all().filter(tags__in=[tag]).count()


@register.inclusion_tag('blog/includes/popular_posts.html')
def show_popular_posts(count=3):
    return {
        'posts': Post.objects.prefetch_related('parts')\
                        .filter(parts=None, status='published')\
                        .order_by('-views', '-created')[:count]
    }


@register.inclusion_tag('blog/includes/subscribe_from.html')
def show_subscribe_form():
    return {
        'subscribe_form': SubscribeForm()
    }


@register.inclusion_tag('blog/includes/tags.html')
def show_tags(count=8):
    return {
        'tags': Post.tags.most_common()[:count]
    }


@register.inclusion_tag('blog/includes/snippets.html')
def top_snippet_tags(count=5):
    return {
        'tags': Snippet.tags.most_common()[:count]
    }


@register.filter
def count_snippets_by_tag(tag):
    return Snippet.objects.all().filter(tags__in=[tag]).count()


@register.filter
def fill_color_tag(tag):
    colors = [
        'badge-primary', 'badge-secondary', 'badge-tertiary', 'badge-info',
        'badge-success', 'badge-warning', 'badge-dark', 'badge-gray', 'badge-light'
    ]
    index = tag.id % len(colors)

    return colors[index]