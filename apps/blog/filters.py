import django_filters

from django import forms
from django.db.models import Q

from .models import Post

from apps.core.models import Category


def filter_title_or_overview(queryset, name, value):
	return queryset.filter(Q(title__contains=value)|Q(overview__contains=value))


def filter_select(queryset, name, value):
	if value == 'new':
		return queryset.order_by('-created')
	elif value == 'xn':
		return queryset.order_by('-views')
	elif value == 'lt':
		return queryset.order_by('-like', '-created')

	return queryset


SELECT_CHOICES = (
	('new', 'Mới nhất'),
	('xn', 'Xem nhiều'),
	('lt', 'Lượt thích'),
)

class PostFilter(django_filters.FilterSet):
	query = django_filters.CharFilter(method=filter_title_or_overview)
	category = django_filters.ModelChoiceFilter(queryset=Category.objects.filter(posts__parts=None).exclude(posts=None).distinct(), widget=forms.Select, empty_label='Tất cả')
	select = django_filters.ChoiceFilter(choices=SELECT_CHOICES, widget=forms.Select, method=filter_select, empty_label='Tất cả')

	class Meta:
		model = Post
		fields = ('category', 'query', 'select', )
