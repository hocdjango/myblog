from django.urls import path


from . import views


urlpatterns = [
	path('blog/', views.post_list, name='post_list'),
	path('blog/tag/<slug:tag_slug>/', views.post_list, name='post_list_by_tag'),
	path('blog/<slug:slug>/', views.post_detail, name='post_detail'),
	
	path('ajax/like/post/<slug:post_slug>/', views.ajax_update_post_like, name='ajax_update_post_like'),
	path('ajax/<int:post_id>/comment/create/', views.create_comment, name='create_comment'),
	path('ajax/comment/remmove/<int:comment_id>/', views.remove_comment, name='remove_comment'),
	path('ajax/comment/update/<int:comment_id>/', views.comment_update, name='comment_update'),
	path('ajax/comment/like/<int:comment_id>/', views.like_comment, name='like_comment'),
	path('ajax/comment/dislike/<int:comment_id>/', views.dislike_comment, name='dislike_comment'),
	path('ajax/comment/reply/<int:comment_id>/', views.comment_reply, name='comment_reply'),
	path('ajax/comment/load/<int:post_id>/', views.load_comment, name='load_comment'),
]
