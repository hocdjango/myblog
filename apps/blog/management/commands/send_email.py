from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.sites.models import Site
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from apps.blog.models import Post
from apps.courses.models import Course
from apps.core.models import Subscribe


class Command(BaseCommand):

    def handle(self, *args, **kwargs):

        post = Post.published.all().first()
        course = Course.published.all().first()
        if post and course:
            instance = post if post.created > course.created else course
        elif post or course:
            instance = post if post else course

        context = {}
        sender = instance._meta.model
        if instance.status == 'published':
            if sender.__name__ == 'Course':
                context['link_text'] = 'Xem khóa học'
                context['img_link'] = instance.image.url
                context['link'] = instance.get_absolute_url()
                context['overview'] = ''
                context['title'] = instance.title
            elif sender.__name__ == 'Post':
                context['link_text'] = 'Xem bài viết'
                context['title'] = instance.title
                context['overview'] = instance.overview
                context['link'] = instance.get_absolute_url()
                img = instance.image
                context['img_link'] = img.url if img else ''

            domain = Site.objects.get_current().domain
            context['link'] = ''.join(('http://', domain, context['link']))

            if context['img_link']:
                subject = context['title']
                html_message = render_to_string('core/email/post.html', context)
                plain_message = strip_tags(html_message)
                from_email = 'djangobat <hocdjango@gmail.com>'
                emails = get_user_model().objects.exclude(email="hocdjango@gmail.com").values_list('email', flat=True)
                subscribes = Subscribe.objects.values_list('email', flat=True)
                emails_to = list(emails.union(subscribes))
                try:
                    self.stdout.write(self.style.HTTP_NOT_MODIFIED(f"Sending ....!"))
                    print(emails_to)
                    send_mail(subject, plain_message, from_email, emails_to, html_message=html_message)
                    self.stdout.write(self.style.SUCCESS("SUCCESS!"))
                except:
                    self.stdout.write(self.style.ERROR("ERROR!"))
                    pass