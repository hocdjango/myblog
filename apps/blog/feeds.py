from django.contrib.syndication.views import Feed
from django.utils.html import mark_safe

from .models import Post


class LatestPostsFeed(Feed):
	title = 'djangobat Blog'
	link = '/blog/'
	description = 'Các bài viết mới của djangobat.'
	
	def items(self):
		return Post.published.all()[:5]

	def item_title(self, item):
		return item.title
	
	def item_description(self, item):
		return item.overview

