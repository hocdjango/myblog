import string
import random

from django.db import models
from django.utils.text import slugify

from .managers import CategoryManager


class BaseContent(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField(max_length=140, blank=True)

	def __str__(self):
		return self.title

	def save(self):
		if not self.slug:
			slug = slugify(self.title)
			hext = ''.join(random.choice(string.hexdigits) for i in range(5))
			self.slug = '-'.join((slug, hext))
		super(BaseContent, self).save()

	class Meta:
		abstract = True


class Category(BaseContent):
	objects = CategoryManager()

	class Meta:
		verbose_name_plural = 'Categories'


class Subscribe(models.Model):
	email = models.EmailField(max_length=50, unique=True)

	def __str__(self):
		return self.email


class Contact(models.Model):
	TYPE_CHOICES = (
		('q', 'Câu hỏi'),
		('da', 'Thuê dự án'),
		('hda', 'Hỗ trợ dự án'),
		('do', 'Dạy online'),
	)
	name = models.CharField(max_length=100)
	contact_type = models.CharField(choices=TYPE_CHOICES, max_length=5, default='q')
	email = models.EmailField()
	content = models.TextField()

	def __str__(self):
		return f'{self.name} '
