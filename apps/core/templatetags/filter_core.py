from django import template
from urllib.parse import urlencode, urlparse, urlunparse, parse_qs, parse_qsl, urlsplit


register = template.Library()


@register.filter
def get_path_without_page(value):
	query_dict = dict(parse_qsl(urlsplit(value).query))

	u = urlparse(value)
	query = parse_qs(u.query, keep_blank_values=True)
	query.pop('page', None)

	u = u._replace(query=urlencode(query, True))
	if query:
		value = urlunparse(u) + '&'
	else:
		value = '?'
		
	return value