from django import forms
from .models import Subscribe, Contact


class SubscribeForm(forms.ModelForm):
	class Meta:
		model = Subscribe
		fields = ('email',)


class ContactForm(forms.ModelForm):

	class Meta:
		model = Contact
		fields = ('name', 'email', 'contact_type', 'content',)