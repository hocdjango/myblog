from django.contrib import admin

from .models import Category, Subscribe, Contact


admin.site.register(Category)
admin.site.register(Subscribe)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
	list_display = ('name', 'email', 'content',)
