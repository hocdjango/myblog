from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class StaticSiteMap(Sitemap):
	priority = 0.5
	changefreq = 'daily'

	def items(self):
		return ['about', 'home']

	def location(self, item):
		return reverse(item)
