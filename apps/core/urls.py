from django.urls import path

from . import views


urlpatterns = [
	path('', views.home, name='home'),
	path('terms/', views.terms, name='terms'),
	path('about/', views.about, name='about'),
	path('contact/', views.contact, name='contact'),
	path('ajax/subscribe/', views.subscribe_submit, name='subscribe_submit'),
]
