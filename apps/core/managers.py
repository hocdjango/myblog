from django.db import models


class CategoryManager(models.Manager):
    def get_queryset(self):
        return super(CategoryManager,
                    self).get_queryset()\
                    .prefetch_related('courses')