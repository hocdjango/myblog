from django.shortcuts import render
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from .forms import SubscribeForm, ContactForm

from apps.courses.models import Course
from apps.blog.models import Post
from apps.core.decorators import ajax_required


def home(request):
	courses = Course.published.all()[:3]
	posts = Post.published.all()[:3]

	context = {
		'section': 'home',
		'courses': courses,
		'posts': posts,
		'subscribe_form': SubscribeForm(),

	}

	return render(request, 'core/home.html', context)


def about(request):
	return render(request, 'core/about.html')


def terms(request):
	return render(request, 'core/terms.html')


def contact(request):
	success = False
	if request.method == "POST":
		form = ContactForm(request.POST)
		if form.is_valid():
			form.save()
			success = True


	form = ContactForm()

	context = {
		'form': form,
		'success': success,
	}

	return render(request, 'core/contact.html', context)


@require_POST
@ajax_required
def subscribe_submit(request):
	print(request.POST['csrfmiddlewaretoken'])
	if request.method == 'POST':
		data = {}
		form = SubscribeForm(request.POST)
		if form.is_valid():
			form.save()
			data['success'] = True
			data['message'] = 'Đăng ký thành công'
		else:
			data['success'] = False
			data['message'] =  "Địa chỉ email này đã subscribe"

		return JsonResponse(data)