from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from .models import Subscribe


@receiver(post_save, sender=Subscribe)
def subscribe_save(sender, instance, created, **kwargs):
    if created:
        subject = "Subscribe trang djangobat.com"
        html_message = render_to_string('core/email/subscribe.html')
        plain_message = strip_tags(html_message)
        from_email = 'djangobat <hocdjango@gmail.com>'
        to = instance.email
        try:
            send_mail(subject, plain_message, from_email, [to], html_message=html_message)
        except:
            print("ERROR! send email error")
